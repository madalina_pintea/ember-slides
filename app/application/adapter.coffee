`import LSAdapter from 'ember-localstorage-adapter'`

ApplicationAdapter = LSAdapter.extend
  namespace: 'ember-slides'

`export default ApplicationAdapter`
