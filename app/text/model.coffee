`import Model from 'ember-data/model'`
`import DS from 'ember-data'`
`import Shape from '../shape/model'`

TextShape =  Shape.extend(
  color: DS.attr 'string'
  content: DS.attr 'string'
  font_family: DS.attr 'string'
  font_size: DS.attr 'number'
  height: DS.attr 'number', {defaultValue: '50'}
  text_align: DS.attr 'string'
  type: DS.attr 'string', {defaultValue: 'text'}
  width: DS.attr 'number', {defaultValue: '200'})

`export default TextShape`
