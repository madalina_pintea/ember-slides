`import Model from 'ember-data/model'`
`import DS from 'ember-data'`
`import Shape from '../shape/model'`

ImageShape = Shape.extend(
  content: DS.attr 'string'
  height: DS.attr 'number', {defaultValue: '200'}
  type: DS.attr 'string', {defaultValue: 'image'}
  width: DS.attr 'number', {defaultValue: '350'})

`export default ImageShape`
