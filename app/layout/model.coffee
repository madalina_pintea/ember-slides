`import DS from 'ember-data'`

Layout = DS.Model.extend(
  note: DS.attr 'string', {defaultValue: ''}
  presentation: DS.belongsTo 'presentation'
  shapes: DS.hasMany 'shape', {polymorphic: true})

`export default Layout`
