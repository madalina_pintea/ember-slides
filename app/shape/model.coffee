`import Model from 'ember-data/model'`
`import DS from 'ember-data'`

Shape =  Model.extend(
  x: DS.attr 'number', {defaultValue: '0'}
  y: DS.attr 'number', {defaultValue: '0'}
  layout: DS.belongsTo 'layout')

`export default Shape`
