`import Model from 'ember-data/model'`
`import DS from 'ember-data'`

Presentation = Model.extend(
  layouts: DS.hasMany 'layout'
  modified_date: DS.attr 'string'
  title: DS.attr 'string')

`export default Presentation`
