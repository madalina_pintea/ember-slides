`import Model from 'ember-data/model'`
`import DS from 'ember-data'`
`import Shape from '../shape/model'`

VideoShape = Shape.extend(
  height: DS.attr 'string', {defaultValue: '315'}
  src: DS.attr 'string'
  type: DS.attr 'string', {defaultValue: 'video'}
  width: DS.attr 'string', {defaultValue: '560'})

`export default VideoShape`
