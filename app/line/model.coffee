`import Model from 'ember-data/model'`
`import DS from 'ember-data'`
`import Shape from '../shape/model'`

LineShape = Shape.extend(
  background_color: DS.attr('string')
  height: DS.attr 'number', {defaultValue: '5'}
  type: DS.attr 'string', {defaultValue: 'line'}
  width: DS.attr 'number', {defaultValue: '100' })

`export default LineShape`
