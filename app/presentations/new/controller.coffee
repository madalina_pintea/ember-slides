`import Ember from 'ember'`

PresentationsNewController = Ember.Controller.extend
  presentationName: ''

  actions:
    navigateToPresentationsList: ->
      @transitionToRoute('presentations')

    createPresentation: ->
      @store.createRecord('presentation', {
        modified_date: moment(new Date()).format("DD-MM-YYYY"),
        title: @get('presentationName') or 'Untitled'
      }).save()
      @transitionToRoute('presentations')

`export default PresentationsNewController`
