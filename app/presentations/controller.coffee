`import Ember from 'ember'`

PresentationsController = Ember.Controller.extend
  actions:
    addEmptyPresentation: ->
      @transitionToRoute('presentations.new')

    handleClickPresentation: (presentation)->
      @transitionToRoute('index', presentation)

`export default PresentationsController`
