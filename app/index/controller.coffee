`import Ember from 'ember'`

IndexController = Ember.Controller.extend

  layoutsFromTheModel: Ember.computed('model', ->
    @get('model').get('layouts')
  )

  actions:
    handleLayoutClick: (layout) ->
      @set('layoutSelected', layout)
      @transitionToRoute('index.canvas', layout.get('id'))

    handleTextButtonClick: (action) ->
      @send 'addShapeToLayout', action

    handleNoteButtonClick: ->
      @toggleProperty 'isNoteVisible'

    handleIconButtonClick: (action) ->
      @send 'alignShape', action

    alignShape: (align)->
      console.log('align' , align)

    addShapeToLayout: (shape) ->
      if @get('layoutSelected')
        @get('layoutSelected').get('shapes').pushObject(shape)
        shape.save()
        @get('layoutSelected').save()

    handleSelectToolbarColor: (color) ->
      console.log('handleToolbarColor : ', color)

    handlePrototypeClick: (prototype) ->
      layoutsFromTheModel =  @get('layoutsFromTheModel')
      if layoutsFromTheModel.get('length') == 0
        @send('addEmptyLayout')

    handlePanelColorChange: (colorLayout) ->
      console.log(colorLayout)

    addEmptyLayout: (layout) ->
      layoutsFromTheModel =  @get('layoutsFromTheModel')
      layout.save()
      @set 'selectedSlide', layout
      @set 'isNoteVisible', false
      layoutsFromTheModel.pushObject(layout)
      @get('model').save()
      @send('handleLayoutClick', layout)

    removeLayout: (layout) ->
      layoutsFromTheModel =  @get('layoutsFromTheModel')
      selectedSlideIndex = layoutsFromTheModel.indexOf(layout)
     
      layoutsFromTheModel.removeObject(layout)
      @store.findRecord('layout', layout.get('id')). then (layout) ->
        layout.destroyRecord()
      @get('model').save()

      lengthOflayoutsFromTheModel =  layoutsFromTheModel.get('length')
      if @get('selectedSlide') == layout && lengthOflayoutsFromTheModel > 0
        if (selectedSlideIndex is not lengthOflayoutsFromTheModel) || (lengthOflayoutsFromTheModel > 1 && selectedSlideIndex is 0)
          @transitionToRoute('index.canvas', layoutsFromTheModel.objectAt(selectedSlideIndex).get('id'))
          @set 'selectedSlide', layoutsFromTheModel.objectAt(selectedSlideIndex)
        else
          @transitionToRoute('index.canvas', layoutsFromTheModel.get('lastObject').get('id'))
          @set 'selectedSlide',layoutsFromTheModel.get('lastObject')

    saveTextareaContent: (content)->
      console.log('The note added is:', content)

`export default IndexController`
