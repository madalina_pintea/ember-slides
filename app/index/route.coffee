`import Ember from 'ember'`

IndexRoute = Ember.Route.extend
  model: (param) ->
    return @store.findRecord('presentation', param.id)

`export default IndexRoute`
