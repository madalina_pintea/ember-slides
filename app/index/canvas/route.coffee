`import Ember from 'ember'`

IndexCanvasRoute = Ember.Route.extend
  model: (param) ->
    Ember.RSVP.hashSettled({
      images: @store.query('image', { layout: param.slide }),
      lines: @store.query('line', { layout: param.slide }),
      texts: @store.query('text', { layout: param.slide })
      })

  setupController: (controller, model) ->
    controller.set('images', model.images.value)
    controller.set('texts', model.texts.value)
    controller.set('lines', model.lines.value)

`export default IndexCanvasRoute`
