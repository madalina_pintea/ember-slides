`import Ember from 'ember'`

IndexPresentationTitleComponent = Ember.Component.extend
  classNames: ['navbar__title-presentation']
  classNameBindings: ['isEditable:navbar__title-presentation--selected:navbar__title-presentation--deselected']
  attributeBindings: ['isEditable:contenteditable']
  isEditable: false
  mouseDown: ->
    @set('isEditable', true)
    @set('holdText', @get('element').innerText)

  keyUp: (key)->
    Esc = 27
    if key.keyCode == Esc
      @get('element').innerText = @get('holdText')
      @set('isEditable', false)

  focusOut: ->
    @set('isEditable', false)
    @set('presentation.title', @get('element').innerText)
    @get('presentation').save()

`export default IndexPresentationTitleComponent`
