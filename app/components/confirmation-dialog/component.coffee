`import Ember from 'ember'`

ConfirmationDialogComponent = Ember.Component.extend
  confirmButtonLabel: 'CONFIRM'
  classNameBindings: ['className']
  classNames: ['dialog']
  closeButtonLabel: 'CLOSE'

`export default ConfirmationDialogComponent`
