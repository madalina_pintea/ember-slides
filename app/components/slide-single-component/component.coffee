`import Ember from 'ember'`

SlideSingleComponentComponent = Ember.Component.extend
  tagName: 'div'
  classNames: ['slide-list__item']
  classNameBindings: ['isSelected:slide-list__item--selected']
  actions:
    handleSlideRemoveClick: () ->
      thisHTML = Ember.get(this, 'element')
      thisHTML.remove()
      @sendAction 'handleSlideRemoveClick'
    handleSlideClick: () ->
      @sendAction 'handleSlideClick'
      
`export default SlideSingleComponentComponent`
