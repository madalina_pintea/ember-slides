`import Ember from 'ember'`

CanvasPanelPropertiesComponent = Ember.Component.extend
  isLayouts: true
  classNames: 'canvas-panel'
  actions:
    clickLayout: ->
      @set('isLayouts', true)
      @set('isProperties', false)
      @toggleProperty('isActive')
      
    clickProperties: ->
      @set('isLayouts', false)
      @set('isProperties', true)
      @toggleProperty('isActive')

`export default CanvasPanelPropertiesComponent`