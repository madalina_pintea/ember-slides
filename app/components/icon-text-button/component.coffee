`import Ember from 'ember'`

IconTextButtonComponent = Ember.Component.extend
  tagName: 'button'
  classNames: ['canvas__button']
  classNameBindings: ['isSelected:canvas__button--selected']

`export default IconTextButtonComponent`
