`import Ember from 'ember'`
`import Buttons from './Buttons'`

CanvasToolbarComponent = Ember.Component.extend
  classNames: ['canvas canvas--toolbar']
  store: Ember.inject.service('store')
  init: ->
    @_super(arguments ...)
    @set 'buttons', Buttons

  actions:
    handleClick: (button) ->
      @set 'holdClickedButton', button
      if button.shapeTypeButton
        shape = @get('store').createRecord(button.type)
        @sendAction 'handleTextButtonClick', shape

      if button.type == 'note'
        @sendAction 'handleNoteButtonClick', shape

      if button.alignButton
        @sendAction 'handleIconButtonClick', button.type

      if button.addSlide
        layout = @get('store').createRecord('layout')
        @set 'selectedSlide', layout
        @sendAction 'addEmptySlide', layout

`export default CanvasToolbarComponent`
