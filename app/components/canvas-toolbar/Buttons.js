import Ember from 'ember';

export default [
  Ember.Object.create({
    addSlide: true,
    icon: 'plus-square-o',
    text: 'Add slide'
  }),
 Ember.Object.create({
    shapeTypeButton: true,
    icon: 'picture-o',
    text: 'Image',
    type: 'image'
  }),
  Ember.Object.create({
    shapeTypeButton: true,
    icon: 'text-height',
    text: 'Text',
    type: 'text'
  }),
  Ember.Object.create({
    alignButton: true,
    icon: 'align-left',
    text: 'Align left',
    type: 'left'
  }),
  Ember.Object.create({
    alignButton: true,
    icon: 'align-center',
    text: 'Align center',
    type: 'center'
  }),
  Ember.Object.create({
    alignButton: true,
    icon: 'align-right',
    text: 'Align right',
    type: 'right'
  }),
  Ember.Object.create({
    shapeTypeButton: true,
    icon: 'arrows-h',
    text: 'Line',
    type: 'line'
  }),
  Ember.Object.create({
    icon: 'sticky-note-o',
    text: 'Note',
    type: 'note'
  })
];
