`import Ember from 'ember'`

ModalContainerComponent = Ember.Component.extend(
  classNames: ['modal-container']
)

`export default ModalContainerComponent`
