`import Ember from 'ember'`

SlideListComponentComponent = Ember.Component.extend
  classNames: ['slide-list']
  store: Ember.inject.service()
  dragulaconfig =
    option :
      direction: 'vertical'
      copy: false
      revertOnSpill: false
      removeOnSpill: false
    enabledEvents: ['drag','drop']

  init: ->
    @_super(arguments ...)
    @set 'selectedSlide', @get('slides').objectAt(0)

  didReceiveAttrs: ->
    if @get('slides').get('length') == 0
      @send 'addEmptySlide'

  actions:
    handleSlideClick: (slideItem) ->
      @set 'selectedSlide', slideItem
      @sendAction 'handleSlideClick', slideItem

    addEmptySlide: ->
      layout = @get('store').createRecord('layout')
      @set 'selectedSlide', layout
      @sendAction 'addEmptySlide', layout

`export default SlideListComponentComponent`
