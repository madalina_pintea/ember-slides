`import { test, moduleForComponent } from 'ember-qunit'`

moduleForComponent 'canvas-panel-properties', 'Unit | Component | canvas panel properties', {
  # Specify the other units that are required for this test
  needs: ['component:layouts-list', 'component:canvas-properties-color-picker'],
  unit: true
}

test 'it renders layouts-list component on Layouts head title click', (assert) ->
  assert.expect 1
  component = @subject()
  @render()
  @$('span')[0].click()
  assert.equal @$('.canvas-panel__layout').length, 1

test 'it renders canvas-properties-color-picker component on Properties head title click', (assert) ->
  assert.expect 1
  component = @subject()
  @render()
  @$('span')[1].click()
  assert.equal @$('.sp-replacer').length, 1

test 'should add class: .active to Properties head title if clicked', (assert) ->
  assert.expect 1
  component = @subject()
  @render()
  @$('span')[1].click()
  assert.equal @$('.canvas-panel__head .canvas-panel__item--active').text(), 'Properties'
