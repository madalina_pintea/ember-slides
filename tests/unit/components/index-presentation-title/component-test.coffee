`import { test, moduleForComponent } from 'ember-qunit'`

moduleForComponent 'index-presentation-title', 'Unit | Component | index presentation title', {
  # Specify the other units that are required for this test
  # needs: ['component:foo', 'helper:bar'],
  unit: true
}

test 'should call mouseDown function from component ', (assert) ->
  assert.expect 1

  component = @subject()
  component.set 'mouseDown', () ->
    assert.ok(true)

  @render()
  @$().mousedown()

test 'should set isEditable : true ', (assert) ->
  assert.expect 1

  component = @subject()

  @render()
  @$().mousedown()
  assert.equal component.get('isEditable'), true

test 'should call keyUp: Esc function from component', (assert) ->
  assert.expect 1

  component = @subject()
  component.set 'keyUp', (key) ->
    if key.keyCode == 27
      assert.ok(true)

  @render()
  @$().trigger( $.Event('keyup',{keyCode: 27}) )

test 'on Esc KeyUp should set isEditable : false ', (assert) ->
  assert.expect 1

  component = @subject()

  @render()
  @$().trigger( $.Event('keyup',{keyCode: 27}) )
  assert.equal component.get('isEditable'), false


test 'should call focusOut function from component', (assert) ->
  assert.expect 1

  component = @subject()
  component.set 'focusOut', (key) ->
    assert.ok(true)

  @render()
  @$().trigger($.Event('focusout'))
