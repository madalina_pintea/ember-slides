`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'layouts-list', 'Unit | Component | layouts list', {
  # Specify the other units that are required for this test
  needs: ['component:button-image', 'helper:eq'],
  unit: true
}

test 'it triggers external action when layout button is clicked', (assert) ->
  component = @subject()

  component.set 'clickPrototype', () ->
    assert.ok(true)

  @render hbs """{{layouts-list}}"""
  @$('button').click()

test 'it applies highlight class', (assert) ->
  component = @subject()
  @render hbs """{{layouts-list}}"""
  @$('button').click()
  assert.equal(@$('.canvas-panel__layout-item--selected').length, 1)
