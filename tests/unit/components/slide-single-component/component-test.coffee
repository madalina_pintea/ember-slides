`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'slide-single-component', 'Unit | Component | slide single component', {
  needs: ['component:slide-list-component', 'component:fa-icon', 'helper:plus-one'],
  unit: true
}

test 'it triggers external action when slide is clicked', (assert) ->
  component = @subject()

  component.set 'handleSlideClick', () ->
    assert.ok(true)  

  @render()
  @$('.slide-list__item-content')[0].click()
  

test 'it triggers action when the Remove slide is clicked', (assert) ->
  component = @subject()

  component.set 'handleSlideRemoveClick', () ->
    assert.ok(true)  

  @render()
  @$('.slide-list__item-content')[0].click()
  @$('.slide-list__item-remove-icon')[0].click()

