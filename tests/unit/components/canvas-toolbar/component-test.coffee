`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'canvas-toolbar', 'Unit | Component | canvas toolbar', {
  needs: ['helper:eq', 'component:icon-text-button', 'component:toolbar-color-picker', 'model:image', 'model:layout', 'model:text', 'model:line', 'model:presentation', 'model:shape'],
  unit: true
}

test 'it triggers click action when icon-text-button is clicked', (assert) ->
  component = @subject()
  component.set 'onColor', () ->

  actions = {
   handleClick: () ->
     assert.ok(true)
   }
  component.set 'actions', actions

  @render hbs """"{{canvas-toolbar }}"""
  @$('.canvas__button').click()

test 'it\'s adding .canvas__button--selected class on button click', (assert) ->
  component = @subject()
  component.set 'onColor', () ->
  @render hbs """"{{canvas-toolbar }}"""
  @$('.canvas__button').click()

  assert.equal(@$('.canvas__button--selected').length, 1)
