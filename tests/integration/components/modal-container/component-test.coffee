`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'modal-container', 'Integration | Component | modal container', {
  integration: true
}

test 'modal container and child dialog', (assert) ->
  assert.expect 2
  @render hbs """{{#modal-container}}
                child-dialog
              {{/modal-container}}"""
  assert.equal @$('.modal-container').length, 1,  'the component has the "modal-container" class'
  assert.equal @$('.modal-container').text().trim(), 'child-dialog',  'child dialog is rendered inside the component'
