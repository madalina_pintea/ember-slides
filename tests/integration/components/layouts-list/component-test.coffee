`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'layouts-list', 'Integration | Component | layouts list', {
  integration: true
}

test 'it renders without template', (assert) ->
  assert.expect 1
  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->
  @render hbs """{{layouts-list}}"""
  assert.equal @$().text().trim(), ''

test 'it renders 10 button images layouts', (assert) ->
  assert.expect 1

  @render hbs """
    {{#layouts-list}}
    {{/layouts-list}}
  """
  assert.equal @$('.canvas-panel__layout-item').length, 10
