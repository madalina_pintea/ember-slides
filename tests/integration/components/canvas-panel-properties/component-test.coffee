`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'canvas-panel-properties', 'Integration | Component | canvas panel properties', {
  integration: true
}

test 'it renders layouts list as default', (assert) ->
  assert.expect 1
  @render hbs """{{canvas-panel-properties}}"""
  assert.equal @$('.canvas-panel__layout').length, 1

test 'should add class: .active to Layouts head title as default', (assert) ->
  assert.expect 1
  @render hbs """{{canvas-panel-properties}}"""

  assert.equal @$('.canvas-panel__head .canvas-panel__item--active').text(), 'Layouts'
