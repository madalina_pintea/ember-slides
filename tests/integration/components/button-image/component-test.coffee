`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'button-image', 'Integration | Component | button image', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 1
  @render hbs """{{button-image}}"""
  assert.equal @$().text().trim(), ''

test 'it renders with template', (assert) ->
  @render hbs """{{#button-image}}
                  template block text
                {{/button-image}}"""
  assert.equal @$().text().trim(), 'template block text'

test 'it renders a img tag', (assert) ->
  @render hbs """{{button-image}}"""
  assert.equal @$('img').length, 1
