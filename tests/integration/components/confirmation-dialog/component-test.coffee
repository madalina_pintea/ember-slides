`import { moduleForComponent, test } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'confirmation-dialog', 'Integration | Component | confirmation dialog', {
  integration: true
}

test 'default confirmation dialog values/state', (assert) ->
  assert.expect 5
  @render hbs """{{confirmation-dialog}}"""
  assert.equal @$('.dialog__header').length, 0,  'dialog header node is not created if title is not sent'
  assert.equal @$('.dialog__content').length, 1,  'dialog content node is rendered even if no content is available'
  assert.equal @$('.dialog__content').text().trim(), '',  'no default dialog content is inserted if the content is not provided'
  assert.equal @$('.dialog__confirm-button').text(), 'CONFIRM',  'default label for action button is "CONFIRM"'
  assert.equal @$('.dialog__close-button').text(), 'CLOSE',  'default label for close button is "CLOSE"'

test 'custom title, content and button labels are used', (assert) ->
  assert.expect 5
  @render hbs """{{#confirmation-dialog
                confirmButtonLabel="CREATE"
                closeButtonLabel="CANCEL"
                title="CREATE PRESENTATION"
              }}content-text{{/confirmation-dialog}}"""
  assert.equal @$('.dialog__header').length, 1,  'dialog header node is created if title is sent'
  assert.equal @$('.dialog__header').text(), 'CREATE PRESENTATION',  'custom title value is used for dialog header'
  assert.equal @$('.dialog__content').text().trim(), 'content-text',  'custom dialog content is inserted inside dialog content node'
  assert.equal @$('.dialog__confirm-button').text(), 'CREATE',  'custom label is used for confirm button'
  assert.equal @$('.dialog__close-button').text(), 'CANCEL',  'custom label is used for close button'

test 'action functions are executed on buttons click', (assert) ->
  assert.expect 2
  @set 'onClose', (event) ->
    assert.equal($(event.target).hasClass('dialog__close-button'), true, 'onClose function is called on close button click')
  @set 'onConfirm', (event) ->
    assert.equal($(event.target).hasClass('dialog__confirm-button'), true, 'onConfirm function is called on confirm button click')

  @render hbs """{{confirmation-dialog
                onClose=onClose
                onConfirm=onConfirm
              }}"""
  @$('.dialog__confirm-button').click()
  @$('.dialog__close-button').click()


