`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'slide-single-component', 'Integration | Component | slide single component', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 1

  @render hbs """{{slide-single-component}}"""

  assert.equal @$().text().trim(), '1'