`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'canvas-toolbar', 'Integration | Component | canvas toolbar', {
  integration: true
}

test 'it renders toolbar buttons and render add slide button', (assert) ->
  @set 'handleToolbarCanvasProperty', () ->
  @render hbs """{{canvas-toolbar onColor=(action handleToolbarCanvasProperty)}}"""
  assert.equal @$('.canvas__button').length, 8

test 'it renders toolbar color-picker', (assert) ->
  @set 'handleToolbarCanvasProperty', () ->
  @render hbs """{{canvas-toolbar onColor =handleToolbarCanvasProperty}}"""
  assert.equal @$('.sp-replacer').length, 1
