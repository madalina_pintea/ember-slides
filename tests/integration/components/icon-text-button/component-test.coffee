`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'icon-text-button', 'Integration | Component | icon text button', {
  integration: true
}

test 'it renders', (assert) ->
  @render hbs """{{icon-text-button}}"""

  assert.equal @$().text().trim(), ''

test 'should add class: canvas__button--selected, if isSelected is true', (assert) ->

  @set 'isSelected', true
  @render hbs """{{icon-text-button}}"""

  assert.equal @$().hasClass('.canvas__button--selected'), true
